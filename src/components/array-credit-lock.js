/**
 * Array Credit Lock
 *
 * Main component for the Array test
 *
 * @returns {JSX.Element}
 * @constructor
 */

import Header from './headers/Header';
import CreditFreezeCenter from './CreditFreezeCenter';
import FactsSteps from './FactsSteps';
import './array-credit-lock.css';

export default function ArrayCreditLock() {
  return (
      <div className="theme-brigit container">
        <Header />

        <div className="contentWrapper">
          <div className="row">
            <div className="column">
              <CreditFreezeCenter />
            </div>
            <div className="column">
              <FactsSteps />
            </div>
          </div>
        </div>
      </div>
  )
}
