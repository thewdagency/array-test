/**
 * Facts Steps
 */

import collapseArrowDown from '../images/collapseArrow-down.svg';
import './factsSteps.css';

export default function FactsSteps() {
  /**
   * Handle fact click
   *
   * @param e
   */
  const handleFactClick = (e) => {
    const targetElem = e.currentTarget;
    const targetAnswer = targetElem.querySelector('.answer');

    if (targetAnswer.classList.contains('show')) {
      targetAnswer.classList.remove('show');
    } else {
      targetAnswer.classList.add('show');
    }
  }

  return (
      <section className="facts-steps">
        <div className="facts-steps-header">
          <h2 className="title">Facts and Steps to Complete a Lock</h2>
          <div className="lock-freeze-box active">
            Credit Lock
          </div>
          <div className="lock-freeze-box">
            Credit Freeze
          </div>
        </div>
        <ul className="questions">
          <li className="collapsible arrow-icon" onClick={handleFactClick}>
            <div
                className="collapsible__open-close-icon open-close-icon"
                data-array-ref="toggleCollapsible"
            >
              <img src={collapseArrowDown} alt="Collapse Arrow Down" />
            </div>

            <div>
              <p
                  className="question collapsible__toggle-handler"
                  data-array-ref="toggleCollapsible"
              >
                <span className="number">1.</span>
                What is TransUnion Credit Lock?
              </p>
              <div className="answer">
                <p className="step-text">
                  Credit Lock is a security measure that prevents creditors from
                  accessing your TransUnion credit information. This blocks
                  banks, credit card companies and more from seeing your credit
                  report, which they typically need to open new accounts for
                  you.
                </p>
                <p className="step-text">
                  A Credit Lock also helps stop identity thieves from opening
                  fraudulent accounts in your name. If you lock your credit,
                  creditors cannot access your credit information, and thieves
                  will be less likely to succeed in leveraging your identity.
                </p>
              </div>
            </div>
          </li>
          <li className="collapsible arrow-icon" onClick={handleFactClick}>
            <div
                className="collapsible__open-close-icon open-close-icon"
                data-array-ref="toggleCollapsible"
            >
              <img src={collapseArrowDown} alt="Collapse Arrow Down" />
            </div>

            <div>
              <p
                  className="question collapsible__toggle-handler"
                  data-array-ref="toggleCollapsible"
              >
                <span className="number">2.</span>
                Why should I lock my credit?
              </p>
              <p className="answer">
                A Credit Lock helps prevent identity thieves from opening
                fraudulent accounts in your name. If you suspect or detect
                fraudulent activity, a Credit Lock is also an important
                mechanism for helping stop additional unwanted activity.
              </p>
            </div>
          </li>
          <li className="collapsible arrow-icon" onClick={handleFactClick}>
            <div
                className="collapsible__open-close-icon open-close-icon"
                data-array-ref="toggleCollapsible"
            >
              <img src={collapseArrowDown} alt="Collapse Arrow Down" />
            </div>

            <div>
              <p
                  className="question collapsible__toggle-handler"
                  data-array-ref="toggleCollapsible"
              >
                <span className="number">3.</span>
                What does TransUnion Credit Lock do? What does it not do?
              </p>
              <div className="answer">
                <p className="sub-step-title">A credit lock will:</p>
                <div className="sub-step-text">
                  <div className="dot"></div>
                  <p className="step-text">
                    Prevent creditors from gaining access to your credit report
                    for most applications
                  </p>
                </div>
                <p className="sub-step-title">A credit lock will not:</p>
                <div className="sub-step-text">
                  <div className="dot"></div>
                  <p className="step-text">
                    Affect your ability to use credit monitoring services
                  </p>
                </div>
                <div className="sub-step-text">
                  <div className="dot"></div>
                  <p className="step-text">
                    Directly impact your credit scores; your scores can still
                    change while your credit is locked
                  </p>
                </div>
                <div className="sub-step-text">
                  <div className="dot"></div>
                  <p className="step-text">
                    Stop you from receiving prescreened offers of credit
                  </p>
                </div>
                <div className="sub-step-text">
                  <div className="dot"></div>
                  <p className="step-text">
                    Prevent you from using existing credit accounts
                  </p>
                </div>
              </div>
            </div>
          </li>
          <li className="collapsible arrow-icon" onClick={handleFactClick}>
            <div
                className="collapsible__open-close-icon open-close-icon"
                data-array-ref="toggleCollapsible"
            >
              <img src={collapseArrowDown} alt="Collapse Arrow Down" />
            </div>

            <div>
              <p
                  className="question collapsible__toggle-handler"
                  data-array-ref="toggleCollapsible"
              >
                <span className="number">4.</span>
                How quickly does a Credit Lock go into effect?
              </p>
              <div className="answer">
                <p className="step-text">
                  The Credit Lock can be placed or lifted instantly.
                </p>
              </div>
            </div>
          </li>
        </ul>
      </section>
  )
}
