/**
 * Credit Freeze Center
 */

import { useEffect, useState } from 'react';
import transunionLogo from '../images/transunion-1.svg';
import svg2 from '../images/svg-2.svg';
import locked from '../images/locked.svg';
import unlocked from '../images/unlocked.svg';
import creditDataFile from '../data/test-1_data.json';
import './creditFreezeCenter.css';

export default function CreditFreezeCenter() {
  const [showHistory, setShowHistory] = useState(false);
  const [showLimitedHistory, setShowLimitedHistory] = useState(true);
  const [creditData, setCreditData] = useState([]);

  useEffect(() => {
    setCreditData(creditDataFile);
  }, []);

  /**
   * Handle show/hide of history
   *
   * @param e
   */
  const handleHistoryShowHide = (e) => {
    setShowHistory(!showHistory);
  }

  /**
   * Handle showing all history
   *
   * @param e
   */
  const showAllHistory = (e) => {
    setShowLimitedHistory(!showLimitedHistory);
  }

  const DisplayCreditData = ({ limit }) => {
    return creditData.map((cred, index) => {
      // Don't display if not for Transunion
      if (cred.provider !== 'tui') {
        return '';
      }

      // Check for limit
      if (limit && index >= 5) {
        return '';
      }

      // Setup date/time format
      const credDate = new Date(cred.date);
      const intlFormat = new Intl.DateTimeFormat('en-US', { dateStyle: 'short', timeStyle: 'long' }).format(credDate);

      // Configure lock status
      let credLockedStatus = true;
      // I was not sure what value constituted a locked/unlocked status
      // so I just used the "type" value since there were differences
      // amongst the data.
      if (cred.type === 'enrollment') {
        credLockedStatus = false;
      }

      return (
        <li className="history-list" key={index}>
          <span className="date">{intlFormat}</span>
          <div className="lock-wrapper">
            <img src={credLockedStatus ? locked : unlocked} alt={credLockedStatus ? 'Locked' : 'Unlocked'} />
            <span className="lock">{credLockedStatus ? 'Locked' : 'Unlocked'}</span>
          </div>
        </li>
      )
    })
  }

  return (
      <section className="credit-freeze-center">
        <ul>
          <div className="center-block active">
            <div className="logo">
              <img src={transunionLogo} alt="Transunion Logo" />
            </div>
            <div className="middle-panel" data-template-id="lockedPanel">
              <div
                  className="unlock-btn"
                  data-template-id="unlockButton"
                  data-array-ref="toggleLockStatus"
              >
                <span className="locked">Locked</span>
                <div className="white-area">
                  <img src={svg2} alt="SVG 2" />
                </div>
              </div>
              <p className="transunion-file">Your TransUnion File is Locked</p>
              <p className="lock-text">
                Unlock your score to open new accounts under your name
              </p>
            </div>
            <div>
              <p className="history-title" onClick={handleHistoryShowHide}>{showHistory ? 'Hide' : 'Show'} lock history</p>
              {showHistory && (
                <>
                  <ul>
                    <DisplayCreditData limit={showLimitedHistory} />
                  </ul>
                  <p className="show-all" onClick={showAllHistory}>{showLimitedHistory ? 'Show All' : 'Hide'} ({creditData.length})</p>
                </>
              )}
            </div>
          </div>
        </ul>
      </section>
  )
}
