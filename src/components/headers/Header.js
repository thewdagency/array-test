/**
 * Header
 *
 * @constructor
 */

import './header.css';

export default function Header() {
  return (
      <header className="credit-lock-header">
        <h2 className="title">Lock your credit</h2>
        <h3 className="subtitle">
          A Credit Lock restricts access to your credit report. Why would you
          lock your credit?
        </h3>
        <ul className="lists">
          <li className="description">
            To prevent creditors from accessing your credit information
          </li>
          <li className="description">
            To help prevent identity thieves from opening fraudulent accounts
            (like credit cards and loans) under your name
          </li>
        </ul>
      </header>
  )
}
