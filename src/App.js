/**
 * App
 *
 * Starting point for the app
 */

import ArrayCreditLock from './components/array-credit-lock';

function App() {
  return (
    <>
      <ArrayCreditLock />
    </>
  );
}

export default App;
