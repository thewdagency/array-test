# Pearl - Frontend Test Application

This application was taken from [https://gitlab.com/array.com/tests-frontend/-/tree/master](https://gitlab.com/array.com/tests-frontend/-/tree/master) and re-built using React (based on create-react-app). The integrity of the application was left 
in tact while going outside the lines a bit and building a component-based application. As a whole, these components can easily be implemented into a currently running React install as needed. 

**Some things to consider**:

1. According to the job description this was mainly a React-based position. If I misunderstood I do apologize as Web Components are not my strong-suit and I am not familiar with writing them and how they are semantically built. I attempted to adapt with this test as best as I could.
2. If this was going into another application, we would need to ensure the environment matches. Some of these are listed below:
   * Node/npm version
   * React/Non-React base
   * If an env file is needed for access to data, etc...
   * Depending on deployment location may need config files for the continuous integration process
3. The base of the application for items such as the favicon and manifest information were left as create-react-app wanted them to be. The focus for me was to convert the page into a working application.
4. Since we could not see the initial design I also took the liberty of making the list and the FAQ into a flexbox 2-column layout. It is also responsive so that at a certain breakpoint it will convert into a single column.

## Install

1. Ensure that you are running at minimum "Node v16.13.1"
2. Pull down the repo using `git clone git@bitbucket.org:thewdagency/array-test.git`
3. Navigate to the root of the install and run `npm install`
5. Start the local server by running `npm start`

## CSS CDN ITEMS

There were a number of CSS files that were referenced from a CDN. These files were imported as a per-component basis using "@import" on the necessary css files for each component. This allows for the styles to be loaded as needed. Since this is a smaller application there was no load-specific chunking required.

## SVG ASSETS

The SVGs were removed from the HTML template and image files were created and add to the "/src/images" folder. This way these files can be imported directly into each component as needed and not duplicated.

## HTML ASSET

The HTML file was broken apart to fit inside each component. See the components section below for reference to these component files.

## COMPONENTS

The new components are set up as follows:

| Filename               | 	Use                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
|------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| array-credit-lock.js	  | - The main part of the application that loads the other components in the correct order <br/>- Also handles the overal layout, whereas in a larger application this would probably be setup within a layout or provider format	                                                                                                                                                                                                                                                                                                                 |
| Header.js	             | - Section that contains the header of the application<br/>- No additional customizations required here                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| CreditFreezeCenter.js	 | - Section that handled the credit freeze box<br/>- Click handler added to show/hide the history list<br/>- Click handler to show/hide all items (based on the "/src/data/test-1_data.json" file)<br/>- When displaying the credit data we made a few assumptions:<br/> ----- We do not display data unless it contains a provider of "tui"<br/>----- Not sure about the audience, so I used the "Intl.dateTimeFormat()" function to format the date/time<br/>----- Assumed the locked/unlocked status was based upon the "type" key in the data |
| FactsSteps.js	         | - Section containing the FAQ<br/>- Removed portions of this data so we could re-write how the collapse worked<br/>- Assumed it was ok to use a CSS-based toggle with transitions (decided to not use an NPM package, but if the application as a whole used it more then we may want to go that route)                                                                                                                                                                                                                                          |
